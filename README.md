# openstack

This repository contains files and templates used to deploy OpenStack in the MBU lab.

OpenStack OverCloud

    API Version: Keystone v3
    Region: regionOne
    Keystone V3 Domain ID: default
    Tenant Mapping Enabled: Yes

Endpoints

    Security Protocol: SSL without validation
    Hostname: overcloud.example.com
    API Port: 13000
    Username: admin
    Password: <secret>

OpenStack UnderCloud

    API Version: Keystone v3
    Keystone V3 Domain ID: default

Endpoints

    Security Protocol: SSL without validation
    Hostname: undercloud.example.com
    API Port: 13000
    Username: admin
    Password: <secret>

Logging

    Hostname: logging.example.com
    Port: 5601
